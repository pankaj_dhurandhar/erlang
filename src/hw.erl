%%%-------------------------------------------------------------------
%%% @copyright (C) 2022
%%% @doc
%%%
%%% @end
%%% Created : 08. Feb 2022 4:21 PM
%%%-------------------------------------------------------------------
-module(hw).
-author("Pankaj Dhurandhar").

%% API
-export([hello_world/0,hello/1]).

hello_world()->
  "Hello World".

%% This function is used for print hello with person name.
%% Here i learned function parameters first letter always capital otherwise through error
%% like :- exception error: no function clause matching
hello(Person_Name) ->
  "Hello " ++ Person_Name.
